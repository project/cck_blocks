Since the git migration, code has been moved into the 6.x-1.x and 7.x-1.x branches.

Please either: 

git checkout 7.x-1.x 

  or 

git checkout 6.x-1.x

depending on your Drupal version.
